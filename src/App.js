import ImageZoom from "./components/ImageZoom";
import image from "./assets/image.jpg";

import "./App.css";

const options = [
  { percentage: 0, scale: 1, x: null, y: null },
  { percentage: 20, scale: 2, x: 50, y: 0 },
  { percentage: 60, scale: 2, x: 100, y: 100 },
  { percentage: 80, scale: 2, x: 0, y: 100 },
  { percentage: 100, scale: 1, x: null, y: null },
];

const App = () => {
  return (
    <div className="App">
      <header className="App-header">
        <ImageZoom
          duration={8}
          options={options}
          height={600}
          width={600}
          image={image}
        />
      </header>
    </div>
  );
};

export default App;
