import React from "react";
import styled, { keyframes } from "styled-components";

const ImageZoom = (props) => {
  const { duration, options, height, width, image } = props;

  const frames = (() => {
    let result;
    options.forEach((element) => {
      const transformOrigin = (() => {
        if (element.x === null) return ``;
        return `transform-origin: ${element.x}% ${element.y}%;`;
      })();

      const currentFrame = `${element.percentage}% {
  transform: scale(${element.scale}); ${transformOrigin}
}`;
      result += currentFrame;
    });
    return keyframes`${result}`;
  })();

  const outerDivStyle = {
    height: `${height}px`,
    width: `${width}px`,
    overflow: "hidden",
  };
  const ImageBlock = styled.div`
    display: inline-block;
    animation: ${frames} ${duration}s ease-out;
    padding: 2rem 1rem;
    font-size: 1.2rem;
    height: ${height}px;
    width: ${width}px;
    background-repeat: no-repeat;
    background-position: center;
    background-size: auto 100%;
    background-image: url("${image}");
  `;
  return (
    <div style={outerDivStyle}>
      <ImageBlock />
    </div>
  );
};

export default ImageZoom;
